with player as (

    select * from {{ ref('stg_player') }}

)

select
    "HKPlayer" as hkplayer,
    player_api_id,
    player_name,
    player_fifa_api_id,
    birthday,
    height,
    weight

from player
