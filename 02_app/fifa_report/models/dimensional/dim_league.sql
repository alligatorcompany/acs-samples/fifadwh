with league as (

    select * from {{ ref('stg_league') }}

)

select
    "HKLeague" as hkleague,
    country_iso as league_country,
    name as league_name

from league
