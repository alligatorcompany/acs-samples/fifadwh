with team as (

    select * from {{ ref('stg_team') }}

)

select
    "HKTeam" as hkteam,
    team_api_id,
    team_fifa_api_id,
    team_long_name,
    team_short_name

from team
