CREATE TABLE IF NOT EXISTS Player_Attributes (
	id	INTEGER PRIMARY KEY,
	player_fifa_api_id	INTEGER,
	player_api_id	INTEGER,
	"date"	VARCHAR(2000000),
	overall_rating	INTEGER,
	potential	INTEGER,
	preferred_foot	VARCHAR(2000000),
	attacking_work_rate	VARCHAR(2000000),
	defensive_work_rate	VARCHAR(2000000),
	crossing	INTEGER,
	finishing	INTEGER,
	heading_accuracy	INTEGER,
	short_passing	INTEGER,
	volleys	INTEGER,
	dribbling	INTEGER,
	curve	INTEGER,
	free_kick_accuracy	INTEGER,
	long_passing	INTEGER,
	ball_control	INTEGER,
	acceleration	INTEGER,
	sprint_speed	INTEGER,
	agility	INTEGER,
	reactions	INTEGER,
	balance	INTEGER,
	shot_power	INTEGER,
	jumping	INTEGER,
	stamina	INTEGER,
	strength	INTEGER,
	long_shots	INTEGER,
	aggression	INTEGER,
	interceptions	INTEGER,
	positioning	INTEGER,
	vision	INTEGER,
	penalties	INTEGER,
	marking	INTEGER,
	standing_tackle	INTEGER,
	sliding_tackle	INTEGER,
	gk_diving	INTEGER,
	gk_handling	INTEGER,
	gk_kicking	INTEGER,
	gk_positioning	INTEGER,
	gk_reflexes	INTEGER
);
CREATE TABLE Player (
	id	INTEGER PRIMARY KEY ,
	player_api_id	INTEGER ,
	player_name	VARCHAR(2000000),
	player_fifa_api_id	INTEGER ,
	birthday	VARCHAR(2000000),
	height	INTEGER,
	weight	INTEGER
);
CREATE TABLE Matches (
	id	INTEGER PRIMARY KEY ,
	country_id	INTEGER,
	league_id	INTEGER,
	season	VARCHAR(2000000),
	stage	INTEGER,
	"date"	VARCHAR(2000000),
	match_api_id	INTEGER ,
	home_team_api_id	INTEGER,
	away_team_api_id	INTEGER,
	home_team_goal	INTEGER,
	away_team_goal	INTEGER,
	home_player_X1	INTEGER,
	home_player_X2	INTEGER,
	home_player_X3	INTEGER,
	home_player_X4	INTEGER,
	home_player_X5	INTEGER,
	home_player_X6	INTEGER,
	home_player_X7	INTEGER,
	home_player_X8	INTEGER,
	home_player_X9	INTEGER,
	home_player_X10	INTEGER,
	home_player_X11	INTEGER,
	away_player_X1	INTEGER,
	away_player_X2	INTEGER,
	away_player_X3	INTEGER,
	away_player_X4	INTEGER,
	away_player_X5	INTEGER,
	away_player_X6	INTEGER,
	away_player_X7	INTEGER,
	away_player_X8	INTEGER,
	away_player_X9	INTEGER,
	away_player_X10	INTEGER,
	away_player_X11	INTEGER,
	home_player_Y1	INTEGER,
	home_player_Y2	INTEGER,
	home_player_Y3	INTEGER,
	home_player_Y4	INTEGER,
	home_player_Y5	INTEGER,
	home_player_Y6	INTEGER,
	home_player_Y7	INTEGER,
	home_player_Y8	INTEGER,
	home_player_Y9	INTEGER,
	home_player_Y10	INTEGER,
	home_player_Y11	INTEGER,
	away_player_Y1	INTEGER,
	away_player_Y2	INTEGER,
	away_player_Y3	INTEGER,
	away_player_Y4	INTEGER,
	away_player_Y5	INTEGER,
	away_player_Y6	INTEGER,
	away_player_Y7	INTEGER,
	away_player_Y8	INTEGER,
	away_player_Y9	INTEGER,
	away_player_Y10	INTEGER,
	away_player_Y11	INTEGER,
	home_player_1	INTEGER,
	home_player_2	INTEGER,
	home_player_3	INTEGER,
	home_player_4	INTEGER,
	home_player_5	INTEGER,
	home_player_6	INTEGER,
	home_player_7	INTEGER,
	home_player_8	INTEGER,
	home_player_9	INTEGER,
	home_player_10	INTEGER,
	home_player_11	INTEGER,
	away_player_1	INTEGER,
	away_player_2	INTEGER,
	away_player_3	INTEGER,
	away_player_4	INTEGER,
	away_player_5	INTEGER,
	away_player_6	INTEGER,
	away_player_7	INTEGER,
	away_player_8	INTEGER,
	away_player_9	INTEGER,
	away_player_10	INTEGER,
	away_player_11	INTEGER,
	goal	VARCHAR(2000000),
	shoton	VARCHAR(2000000),
	shotoff	VARCHAR(2000000),
	foulcommit	VARCHAR(2000000),
	card	VARCHAR(2000000),
	cross	VARCHAR(2000000),
	corner	VARCHAR(2000000),
	possession	VARCHAR(2000000),
	B365H	NUMERIC,
	B365D	NUMERIC,
	B365A	NUMERIC,
	BWH	NUMERIC,
	BWD	NUMERIC,
	BWA	NUMERIC,
	IWH	NUMERIC,
	IWD	NUMERIC,
	IWA	NUMERIC,
	LBH	NUMERIC,
	LBD	NUMERIC,
	LBA	NUMERIC,
	PSH	NUMERIC,
	PSD	NUMERIC,
	PSA	NUMERIC,
	WHH	NUMERIC,
	WHD	NUMERIC,
	WHA	NUMERIC,
	SJH	NUMERIC,
	SJD	NUMERIC,
	SJA	NUMERIC,
	VCH	NUMERIC,
	VCD	NUMERIC,
	VCA	NUMERIC,
	GBH	NUMERIC,
	GBD	NUMERIC,
	GBA	NUMERIC,
	BSH	NUMERIC,
	BSD	NUMERIC,
	BSA	NUMERIC
);
CREATE TABLE League (
	id	INTEGER PRIMARY KEY ,
	country_id	INTEGER,
	name	VARCHAR(2000000) 
);
CREATE TABLE Country (
	id	INTEGER PRIMARY KEY ,
	name	VARCHAR(2000000) 
);
CREATE TABLE IF NOT EXISTS Team (
	id	INTEGER PRIMARY KEY ,
	team_api_id	INTEGER ,
	team_fifa_api_id	INTEGER,
	team_long_name	VARCHAR(2000000),
	team_short_name	VARCHAR(2000000)
);
CREATE TABLE Team_Attributes (
	id	INTEGER PRIMARY KEY ,
	team_fifa_api_id	INTEGER,
	team_api_id	INTEGER,
	"date"	VARCHAR(2000000),
	buildUpPlaySpeed	INTEGER,
	buildUpPlaySpeedClass	VARCHAR(2000000),
	buildUpPlayDribbling	INTEGER,
	buildUpPlayDribblingClass	VARCHAR(2000000),
	buildUpPlayPassing	INTEGER,
	buildUpPlayPassingClass	VARCHAR(2000000),
	buildUpPlayPositioningClass	VARCHAR(2000000),
	chanceCreationPassing	INTEGER,
	chanceCreationPassingClass	VARCHAR(2000000),
	chanceCreationCrossing	INTEGER,
	chanceCreationCrossingClass	VARCHAR(2000000),
	chanceCreationShooting	INTEGER,
	chanceCreationShootingClass	VARCHAR(2000000),
	chanceCreationPositioningClass	VARCHAR(2000000),
	defencePressure	INTEGER,
	defencePressureClass	VARCHAR(2000000),
	defenceAggression	INTEGER,
	defenceAggressionClass	VARCHAR(2000000),
	defenceTeamWidth	INTEGER,
	defenceTeamWidthClass	VARCHAR(2000000),
	defenceDefenderLineClass	VARCHAR(2000000)
);


IMPORT INTO country FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/country.csv' SKIP=1;
IMPORT INTO league FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/league.csv' SKIP=1;

IMPORT INTO player FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/player.csv' SKIP=1;
IMPORT INTO team FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/team.csv' SKIP=1;
IMPORT INTO matches FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/match.csv' SKIP=1;

IMPORT INTO player_attributes FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/player_attributes.csv' SKIP=1;
IMPORT INTO team_attributes FROM LOCAL CSV FILE '/home/torsten/snap/dbeaver-ce/131/.local/share/DBeaverData/workspace6/team_attributes.csv' SKIP=1;





skip 1;