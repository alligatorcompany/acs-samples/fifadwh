
{{
    config(
        materialized='incremental'
    )
}}

with 

fifa_player_attributes as (

    select 
	*
	from {{ ref('fifa_player_attributes') }} 
),

fifa_player as (

    select 
	*
	from {{ ref('fifa_player') }} 
),

fifa_team_attributes as (

    select 
	*
	from {{ ref('fifa_team_attributes') }} 
),

fifa_matches as (

    select 
	*
	from {{ ref('fifa_matches') }} 
),

fifa_team as (

    select 
	*
	from {{ ref('fifa_team') }} 
),

fifa_country as (

    select 
	*
	from {{ ref('fifa_country') }} 
),

fifa_league as (

    select 
	*
	from {{ ref('fifa_league') }} 
),

combined as (

    
    select * from fifa_player_attributes
    union all
    select * from fifa_player
    union all
    select * from fifa_team_attributes
    union all
    select * from fifa_matches
    union all
    select * from fifa_team
    union all
    select * from fifa_country
    union all
    select * from fifa_league
    
{% if is_incremental() %}
),

psa_latest as (

    select ldts,source_system, interface_name, index_col, bk, payload from 
    (
        select psa.*,
            row_number() over (partition by source_system, interface_name, bk order by ldts desc) as ranked 
        from
            {{this}} as psa
    ) where ranked = 1
{% endif %}
)

select
    
    current_timestamp() as ldts, source_system, interface_name, index_col, bk, payload

from combined

{% if is_incremental() %}
where not exists 
(
    select 
        1 
    from psa_latest target 
    where 
        target.source_system=combined.source_system and 
        target.interface_name=combined.interface_name and
        target.index_col=combined.index_col and
        target.bk=combined.bk and 
        target.payload=combined.payload 

)
{% endif %}