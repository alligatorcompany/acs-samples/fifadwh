do-everything:
	PRJ=./00_src/psa && dbt deps --project-dir $$PRJ \
		&& dbt seed --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./00_src/uefa_hr && dbt deps --project-dir $$PRJ && \
		dbt run --target ci --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt test --target ci --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./02_app/fifa && dbt deps --project-dir $$PRJ && \
		dbt seed --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt run --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt test --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./02_app/uefa_dim && dbt deps --project-dir $$PRJ && \
		dbt seed --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt run --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt test --project-dir $$PRJ --profiles-dir $$PRJ
